using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XFPTest.Views;

[assembly: XamlCompilation (XamlCompilationOptions.Compile)]
namespace XFPTest
{
	public partial class App : Application
	{
		public App ()
		{
			InitializeComponent();

			MainPage = new NavigationPage(new TestPage());
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
